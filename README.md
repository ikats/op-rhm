## RHM Operator User Manual

Date: 13th September 2018
Revision: V 0.1
Author: S. Alkhoury
Project: IKATS

### 1. Introduction

This document is a description on how to use the operator RHM (pattern recognition algorithm) with the presentation of the its various parameters.

The program directory is structured in the following way:

> rhm/
>
> - core/
> - learningset/
> - timeseries/
> - main.py

- code: contains the implementation of the main RHM class
- learningset: contains the learningset class/builder used in RHM
- timeseries: contains the timeseries class/builder used in RHM
- main.py: entry point for IKATS (rhm_run)

### 2. The RHM Operator

RHM is a pattern extraction algorithm that is designed to work on datasets of timeseries, it can be used in a supervised or unsupervised context depending on the evaluation function that is used.
The algorithm is also able to handle timeseries of different length (different number of events).

The current implementation is used in a supervised context (labels information are needed).

In the next section, a brief description of the various inputs/parameters that RHM uses.

##### 2.1 Input

The RHM operators accepts the following inputs:

- **learningset**: Timeseries references to be used for RHM computation

##### 2.2 Parameter list

- ***bins***: the first step in RHM is the discretization step, the bins parameter represent the number of horizontal bins that each timeseries will be discretized into
- ***labels***: the set of labels to learn patterns for
- ***windows***: the windows set describes the size of the learned pattern
- ***purity***: All learned patterns should exceed the purity percentage, (purity 1.0 means no noise, pure patterns) [0;1]
- ***cov_threshold***: All learned patterns should cover a required percentage of TS (decreases the number of false positive)
- ***generalizer***: Invoke the generalization step (merging two patterns into one)
- ***var_pair***: Generate patterns for pair of variables, only works with even window length
- ***conf_threshold***: Combine the patterns kept after the tiling (greedy) steps with the most confident patterns (percentage value)

In the following, a table that contains the parameters and their default and possible values:

| 	Key 						| Default value 	|  Possible values 
| ------------------------------------	| --------------------- 	| 	------------- 	
| bins						| 	4			| 	integer		
| labels						| 	empty		| 	learningset labels (text separated by ',')
| windows					| 	empty		| 	text separated by ',' 
| purity						|  	0.9			| 	[0, 1]
| cov_threshold				|   	0.1			|	[0, 1]
| generalizer					|   	False		|	{True, False}
| var_pair	    				|   	False		|	{True, False}
| conf_threshold				|   	0.2			|	[0, 1]



**2.3 Output**

The output of the algorithm is a json structure that contains all information needed to visualize the patterns on their relative timeseries.