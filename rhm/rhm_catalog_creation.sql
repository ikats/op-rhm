-- Align sequences
SELECT setval('catalogue_algorithm_id_seq', COALESCE((SELECT MAX(id)+1 FROM catalogue_algorithmdao), 1), false);
SELECT setval('catalogue_functionalfamily_id_seq', COALESCE((SELECT MAX(id)+1 FROM catalogue_functionalfamilydao), 1), false);
SELECT setval('catalogue_implementation_id_seq', COALESCE((SELECT MAX(id)+1 FROM catalogue_implementationdao), 1), false);
SELECT setval('catalogue_implementationdao_input_desc_items_id_seq', COALESCE((SELECT MAX(id)+1 FROM catalogue_implementationdao_input_desc_items), 1), false);
SELECT setval('catalogue_implementationdao_output_desc_items_id_seq', COALESCE((SELECT MAX(id)+1 FROM catalogue_implementationdao_output_desc_items), 1), false);
SELECT setval('catalogue_profileitemdao_id_seq', COALESCE((SELECT MAX(id)+1 FROM catalogue_profileitemdao), 1), false);

-- Delete orphans
DELETE FROM catalogue_profileitemdao
WHERE id not in
(
   SELECT profileitemdao_id FROM catalogue_implementationdao_input_desc_items
   UNION
   SELECT profileitemdao_id FROM catalogue_implementationdao_output_desc_items
);
DELETE FROM catalogue_implementationdao_input_desc_items WHERE implementationdao_id IN (SELECT id FROM catalogue_implementationdao WHERE name like 'RHM');
DELETE FROM catalogue_implementationdao_output_desc_items WHERE implementationdao_id IN (SELECT id FROM catalogue_implementationdao WHERE name like 'RHM');
DELETE FROM catalogue_implementationdao WHERE name like 'RHM';
DELETE FROM catalogue_algorithmdao WHERE name like 'pattern_extraction';
DELETE FROM catalogue_functionalfamilydao WHERE name like 'supervised_learning';
DELETE FROM catalogue_profileitemdao
WHERE id not in
(
   SELECT profileitemdao_id FROM catalogue_implementationdao_input_desc_items
   UNION
   SELECT profileitemdao_id FROM catalogue_implementationdao_output_desc_items
);

-- PROFILE ITEMS
-- Create profile items
-- direction :
--   0: input/param
--   1: output
-- dtype:
--   0: internal -> parameter
--   1: external -> input/output
INSERT INTO catalogue_profileitemdao
("name", "label", "desc", "direction", "dtype", "order_index", "data_format", "default_value")
VALUES
   ('learningset', 'Learningset', 'Timeseries references to use for RHM computation', 0, 1, 0, 'table', NULL),
   ('bins', 'Discretizing bins', 'number of bins to be used in discretizing the values of the timeseries', 0, 0, 1, 'number', 4),
   ('labels', 'Class labels', 'The set of labels to learn their respective patterns. (the labels should be separated by ",", and if not provided, all labels will be considered)', 0, 0, 2, 'text', NULL),
   ('windows', 'Sliding window', 'the windows set describes the size of the learned pattern (list of integer separated by ",")', 0, 0, 3, 'text', '"4,6"'),
   ('purity', 'Purity percentage', 'All learned patterns should exceed the purity percentage, (positivity 1.0 means no noise, pure patterns) [0;1]', 0, 0, 4, 'number', 0.9),
   ('cov_threshold', 'Coverage threshold', 'All learned patterns should cover a required percentage of TS, (decreases the number of false positive) [0-1]', 0, 0, 5, 'number', 0.1),
   ('generalizer', 'Pattern generalizer', 'Invoke the generalization step (generalizing two patterns into one)', 0, 0, 6, 'bool', false),
   ('var_pair', 'Patterns for pair of variables', 'Generate patterns for pair of variables, only works with even window length', 0, 0, 7, 'bool', false),
   ('conf_threshold', 'Confidence threshold', 'Combine the patterns kept after the tiling step (greedy) with the most confident patterns (percentage value)  [0-1]', 0, 0, 8, 'number', 0.2),
   ('rhm_result', 'Result', 'RHM results', 1, 1, 0, 'RHM_result', NULL);


-- FAMILY
INSERT INTO catalogue_functionalfamilydao
(name, label, "desc")
VALUES
(
   'supervised_learning',
   'Data modeling/Supervised Learning',
   'This family contains all supervised learning algorithms'
);


-- ALGORITHM
INSERT INTO catalogue_algorithmdao
(name, label, "desc", family_id)
VALUES
(
   'pattern_extraction',
   'Pattern Extraction',
   'This algorithm aims to extract patterns from the dataset which describe the traits of each class, the model (in terms of patterns) is learned in a one-against-all scenario.',
   (SELECT id FROM catalogue_functionalfamilydao WHERE name like 'supervised_learning')
);


-- IMPLEMENTATION
INSERT INTO catalogue_implementationdao
(name, label, "desc", algo_id, execution_plugin, library_address, visibility)
VALUES
(
   'RHM',
   'RHM',
   'RHM follows the inductive reasoning approach, at first, the algorithm learns the most specific patterns of each class, then it tries to find more general ones. At the last step, we use a greedy approach to keep a subset of patterns that represent each class',
   (SELECT id FROM catalogue_algorithmdao WHERE name like 'pattern_extraction'),
   'apps.algo.execute.models.business.python_local_exec_engine::PythonLocalExecEngine',
   'LIG.rhm.main::rhm_run',
   TRUE
);

-- Bind them to implementation (Get First id)
INSERT INTO catalogue_implementationdao_input_desc_items ("implementationdao_id", "profileitemdao_id")
VALUES
   ((SELECT id FROM catalogue_implementationdao WHERE name like 'RHM'),(SELECT id FROM catalogue_profileitemdao WHERE "desc" like 'Timeseries references to use for RHM computation' LIMIT 1)),
   ((SELECT id FROM catalogue_implementationdao WHERE name like 'RHM'),(SELECT id+1 FROM catalogue_profileitemdao WHERE "desc" like 'Timeseries references to use for RHM computation' LIMIT 1)),
   ((SELECT id FROM catalogue_implementationdao WHERE name like 'RHM'),(SELECT id+2 FROM catalogue_profileitemdao WHERE "desc" like 'Timeseries references to use for RHM computation' LIMIT 1)),
   ((SELECT id FROM catalogue_implementationdao WHERE name like 'RHM'),(SELECT id+3 FROM catalogue_profileitemdao WHERE "desc" like 'Timeseries references to use for RHM computation' LIMIT 1)),
   ((SELECT id FROM catalogue_implementationdao WHERE name like 'RHM'),(SELECT id+4 FROM catalogue_profileitemdao WHERE "desc" like 'Timeseries references to use for RHM computation' LIMIT 1)),
   ((SELECT id FROM catalogue_implementationdao WHERE name like 'RHM'),(SELECT id+5 FROM catalogue_profileitemdao WHERE "desc" like 'Timeseries references to use for RHM computation' LIMIT 1)),
   ((SELECT id FROM catalogue_implementationdao WHERE name like 'RHM'),(SELECT id+6 FROM catalogue_profileitemdao WHERE "desc" like 'Timeseries references to use for RHM computation' LIMIT 1)),
   ((SELECT id FROM catalogue_implementationdao WHERE name like 'RHM'),(SELECT id+7 FROM catalogue_profileitemdao WHERE "desc" like 'Timeseries references to use for RHM computation' LIMIT 1)),
   ((SELECT id FROM catalogue_implementationdao WHERE name like 'RHM'),(SELECT id+8 FROM catalogue_profileitemdao WHERE "desc" like 'Timeseries references to use for RHM computation' LIMIT 1));
INSERT INTO catalogue_implementationdao_output_desc_items ("implementationdao_id", "profileitemdao_id")
VALUES
   ((SELECT id FROM catalogue_implementationdao WHERE name like 'RHM'),(SELECT id FROM catalogue_profileitemdao WHERE "desc" like 'RHM results' LIMIT 1));
